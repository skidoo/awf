AWF - A Widget Factory
======================

Description
-----------

A widget factory is a theme preview application for gtk2 and gtk3.
It displays the various widget types provided by gtk2/gtk3 into a single window, allowing to see the visual effect of the applied theme.

Features
--------

  * Display widget types available in gtk2/gtk3 (remark: the "option menu" widget type exists only in gtk2; "switch" and "level bar" widget types exist only in gtk3)
  * Menus to select the themes available either at system level or user level ( /usr/share/themes/ or ~/.local/share/themes/ )
  * Toolbar button to start the other gtk version of the tool
  * Toolbar button to refresh the current theme (not working anymore in latest versions of gtk3)
  * Refresh the current theme on SIGHUP

Screenshots
----------

![A widget factory](https://gitlab.com/skidoo/awf/raw/master/awf-screenshot.png)

![AWF previewing a gtk theme which provides both gtk2 and gtk3 versions](https://gitlab.com/skidoo/awf/raw/master/awf-screenshot2.png)


Installation from source (non-Debian instructions)
------------------------

    ./autogen.sh
    ./configure
    make
    make install

Dependencies
------------

  * gtk2 version 2.24
  * gtk3


License
-------

  A widget factory is provided under the terms of the GNU GPLv3 license.
  See the [COPYING](https://gitlab.com/skidoo/COPYING) file for details.

Author
------

  Valère Monseur (valere dot monseur at ymail dot com)

See Also
--------

AWF was created a long time ago when neither gtk2 nor gtk3 provided such a tool.
As part of gtk3 there is now [gtk3-widget-factory](https://developer.gnome.org/gtk3/stable/gtk3-widget-factory.html) and gtk4 will provide [gtk4-widget-factory](https://developer.gnome.org/gtk4/stable/gtk4-widget-factory.html)


