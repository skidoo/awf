awf (1.4.1) unstable; urgency=low

  * retrieved v1.4.0 from github project page Aug 2019  https://github.com/valr/awf
    as mentioned in the 1.4.0 changelog (2017-05-26):
    Merge PR #8: Refresh theme on SIGHUP
    Merge PR #9: Replace some gtk_container_add()s with gtk_box_pack_start()s
  * CHECKED: project has no active forks, no open issue tickets, no open pull requests
  * NOTED:  awf-gtk2 warning if /usr/share/themes/Raliegh/ has been removed
     and /usr/share/themes/Default/gtk/gtkrc does not specify an alternative default theme name
    "No default theme found (neither "Default" nor "Raleigh"), refresh of theme might not work."
  * TODO: (awf.c) merge modded update_progressbar() from  https://github.com/cheese1/awf/blob/master/src/awf.c
  * added statusbar tip:
    TIP: to view warnings related to bugs//deprecations inherent with a given theme,
    LAUNCH awf-gtk2 and/or awf-gtk3 FROM TERMINAL EMULATOR COMMANDLINE

 -- skidoo <email@redact.ed>  Tue, 27 Aug 2019 12:22:22 +0200

awf (1.3.1-2) UNRELEASED; urgency=medium

  * Update debian/* packaging.

 -- Martin Wimpress <code@flexion.org>  Wed, 12 Oct 2016 18:42:24 +0100

awf (1.3.1-1) unstable; urgency=low

  * fix issue #5: synchro progress bars between gtk2 and gtk3 versions of awf.
  * fix COPYING.
  * change desktop category into 'Utilities'.

 -- Valère Monseur <valere.monseur@ymail.com>  Wed, 03 Aug 2016 21:52:00 +0100

awf (1.3.0-1) unstable; urgency=low

  * System and user themes are sorted alphabetically in menus.
  * Added a check button to show text of progress bars.
  * Added font, color and file chooser buttons.
  * Reworked link and switch buttons.
  * Added icon in scale button.
  * Added level bar in gtk3 version.
  * Duplicated the treeview, one with and another without scrollbars.

 -- Valère Monseur <valere.monseur@ymail.com>  Sun, 26 Jan 2014 21:05:00 +0100

awf (1.2.1-2) unstable; urgency=low

  * Initial release

 -- Valère Monseur <valere.monseur@ymail.com>  Thu, 30 Oct 2012 22:44:00 +0100
